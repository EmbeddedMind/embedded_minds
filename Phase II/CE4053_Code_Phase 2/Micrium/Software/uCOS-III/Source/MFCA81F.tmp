/*        
*********************************************************************************************************
                                      OS_RecTask.c
			Functionality for implementing recursive tasks
					
This file includes:
	* skip list data structure definition and functions for manipulating a skip list
	* functions for creating and deleting recursive tasks, based on skip lists
        * AVL tree data structure definition and functions for manipulating an AVL tree
        * functions for implementing EDF scheduling, based on an AVL tree
	
Task recursion is implemented using a skip list.
Tasks are added in increasing order of deadline to a skip list.
Each skip list node contains the time value for that node and a linked list describing tasks
to be created at that time.
Every second, the head of the skip list is checked for any tasks that should be released.
After that is done, the header is deleted.
The header node is always the node closest to the current time.
*********************************************************************************************************
*/
#include <os_RecTask.h>

		
/*
*********************************************************************************************************
*  	OS_REC_SKIPLIST* OSRecTaskInit()
*	Initializes the skip list used for managing recursive tasks, named RecTaskList
*	Returns pointer to RecTaskList
*	If an application requires task recursion, this function should be called before OSRecTaskCreate(),
*	to create the necessary skip list first
*********************************************************************************************************
*/

void OSRecTaskInit(OS_REC_SKIPLIST *RecTaskList)
{
    OS_ERR err;

    OSMemCreate((OS_MEM *)&SkipListForwardPartition,
                (CPU_CHAR *)"SkipList Forward Partition",
                (void *)&SkipListForwardStorage[0],
                (OS_MEM_QTY)APP_TASK_MAX,
                (OS_MEM_SIZE)sizeof(OS_RECNODE) * (SKIPLIST_MAX_LEVEL + 1),
                (OS_ERR *)&err);
    
    OSMemCreate((OS_MEM *)&RecursiveNodePartition,
                (CPU_CHAR *)"Recursive Node Partition",
                (void *)&RecursiveNodeStorage[0],
                (OS_MEM_QTY)APP_TASK_MAX,
                (OS_MEM_SIZE)sizeof(OS_RECNODE),
                (OS_ERR *)&err);
          
    OSMemCreate((OS_MEM *)&RecursiveTaskInfoPartition,
                (CPU_CHAR *)"Recursive Task Info Partition",
                (void *)&RecursiveTaskInfoStorage[0],
                (OS_MEM_QTY)APP_TASK_MAX,
                (OS_MEM_SIZE)sizeof(OS_RECTASKINFO),
                (OS_ERR *)&err);
  
    RecTaskList = skiplist_init(RecTaskList);
	
}

/*
*********************************************************************************************************
void OSRecTaskCreate   (OS_TCB 	   	   *p_tcb, 
                        CPU_CHAR   	   *p_name,
                        OS_TASK_PTR 	p_task,
                        void	   	   *p_arg,
                        OS_PRIO			prio,
                        CPU_STK        *p_stk_base,
                        CPU_STK_SIZE   	stk_limit,
                        CPU_STK_SIZE    stk_size,
                        OS_MSG_QTY     	q_size,
                        OS_TICK        	time_quanta,
                        void           *p_ext,
                        OS_OPT         	opt,
                        OS_ERR         *p_err,
                        int		p_period,
                        OS_REC_SKIPLIST	   *RecTaskList)

Creates the task p_task for its initial release using the standard OSTaskCreate() call, 
and adds an entry for it to the recursive task skip list, for subsequent periodic releases. 

Arguments:
This function uses the same arguments as OSTaskCreate.
It should be called using the same arguments and ordering as you would for OSTaskCreate().
p_period: 	the period of the task (in integer seconds)
RecTaskList: pointer to recursive task skip list

*********************************************************************************************************
*/
void OSRecTaskCreate   (OS_TCB 	   	   *p_tcb, 
						CPU_CHAR   	   *p_name,
						OS_TASK_PTR 	p_task,
						void	   	   *p_arg,
						OS_PRIO			prio,
						CPU_STK        *p_stk_base,
						CPU_STK_SIZE   	stk_limit,
						CPU_STK_SIZE    stk_size,
						OS_MSG_QTY     	q_size,
						OS_TICK        	time_quanta,
						void           *p_ext,
						OS_OPT         	opt,
						OS_ERR         *p_err,
						int		p_period,
						OS_REC_SKIPLIST	   *RecTaskList)
{

	OSTaskCreate(	p_tcb,  			/* Creating initial release of task */
					p_name,  
					p_task, 
					p_arg,  
					prio, 
					p_stk_base,   
					stk_limit,  
					stk_size, 
					q_size,  
					time_quanta, 
					p_ext, 
					opt, 
					p_err);
    
	/* creating of OS_RECTASKINFO structure for skip list insertion */
	OS_RECTASKINFO* task_details = (OS_RECTASKINFO *) malloc(sizeof(OS_RECTASKINFO));
	task_details -> p_tcb = p_tcb;
	task_details -> p_name = p_name;
	task_details -> p_task = p_task;
	task_details -> p_arg = p_arg;
	task_details -> prio = prio;
	task_details -> p_stk_base = p_stk_base;
	task_details -> stk_limit = stk_limit;
	task_details -> stk_size = stk_size;
	task_details -> q_size = q_size;
	task_details -> time_quanta = time_quanta;
	task_details -> p_ext = p_ext;
	task_details -> opt = opt;
	task_details -> p_err = p_err;
	task_details -> p_period = p_period;
	task_details -> next = NULL;
	
	/* insert new task into recursive task skip list */
	skiplist_insert(RecTaskList, p_period, task_details);
}

/*
*********************************************************************************************************
void OSRecTaskMgr	(OS_TMR *p_tmr, void *RecTaskList)

This function is used as a callback function for the periodic timer which expires every 1 second.
When called, the recursive task list is checked at the header node, to see if there are tasks to be released. 
If there are, the tasks are created and new entries are inserted in the skip list for the next release of these tasks.
The header node is then deleted.

Arguments:
p_tmr: pointer to the timer whose callback function is this function. Not used here, but a requirement
        of the OSTmrCreate() API
p_arg: pointer to recursive task skip list. This has to be supplied in the p_arg argument when
             the timer is created.			
*********************************************************************************************************
*/
void OSRecTaskMgr       (OS_TMR *p_tmr, void *p_arg)
{
	OS_ERR *p_err;
	static int current_time = 0;
	OS_RECTASKINFO *task_to_release;
        OS_REC_SKIPLIST *RecTaskList = (OS_REC_SKIPLIST *)p_arg;
		
        current_time++;
        
        /* if there are tasks to be released at this time,
                the skip list header node's time value would be 
                equal to the current time */
        if (RecTaskList -> header -> time == current_time) 
        {
                /* create and release tasks as listed in header node,
                        and insert skip list node for next release of these tasks*/
                task_to_release = RecTaskList -> header -> task;
                while (task_to_release != NULL)
                {
                        OSTaskCreate (	task_to_release -> p_tcb,
                                        task_to_release -> p_name,
                                        task_to_release -> p_task,
                                        task_to_release -> p_arg,
                                        task_to_release -> prio,
                                        task_to_release -> p_stk_base,
                                        task_to_release -> stk_limit,
                                        task_to_release -> stk_size,
                                        task_to_release -> q_size,
                                        task_to_release -> time_quanta,
                                        task_to_release -> p_ext,
                                        task_to_release -> opt,
                                        task_to_release -> p_err);
                        
                        /* In inserting the skip list node for the task's next release,
                                it is assumed that the OS_RECTASKINFO structure will not be deleted.
                                This removes the need for duplicating this OS_RECTASKINFO structure
                                before wiping it out, which means more malloc() and free() calls
                                and possibly more memory fragmentation. If this assumption is wrong,
                                the function for deleting a node in the skip list will need to be
                                altered.
                                
                                Using this assumption, when inserting the new skip list node,
                                the same pointer to a OS_RECTASKINFO structure can be used throughout
                                for each task. The only value in this structure that should change throughout
                                new insertions is the pointer to the next OS_RECTASKINFO structure, but this
                                is taken care of in the append_TCB() function, as all newly-appended structures
                                have their next pointer values set to NULL since they are appended at the end.
                        */
                        
                        skiplist_insert(RecTaskList, 
                                        current_time + task_to_release -> p_period, 
                                        task_to_release);
                        
                        task_to_release = task_to_release -> next;
                }	/* all tasks created, next releases scheduled in skip list */
                
                /* delete current header node */
                skiplist_delete(RecTaskList, current_time);
        }
        
        /* cycle complete, delay for 1 second and then check again */
}

OS_REC_SKIPLIST *skiplist_init(OS_REC_SKIPLIST *list)
{
    int i;
    OS_ERR err;
    
    //Initialization of the Recursive Skiplist pointer
    list->level = 1;
    //Creation of first node of Skiplist
    OS_RECNODE *header = (OS_RECNODE *) OSMemGet((OS_MEM *)&RecursiveNodePartition,
				                 (OS_ERR *)&err);
    
    list->header = header;
    
    //Initialization of the first node of the Skiplist
    header->time = 0;
    header->task = NULL;
    
    //Creation & initilization of 1st level of Skip List array OS_RECNODE
    header->forward = (OS_RECNODE **) OSMemGet((OS_MEM *)&SkipListForwardPartition,
				               (OS_ERR *)&err);

    for (i = 0; i <= SKIPLIST_MAX_LEVEL;i++) {
      header->forward[i] = NULL;
    }
    
    return list;
    
}

void skiplist_insert(OS_REC_SKIPLIST *list, int time, OS_RECTASKINFO *task)
{
    int level;
    OS_RECNODE *update[SKIPLIST_MAX_LEVEL + 1];
    OS_RECNODE *temp = list->header;
    OS_ERR err;
    int i;
    
    //traverse the Skiplist to the specific time node
    //if time node is not created, traverse to the place before the time node should be created
    for (i = list->level;i >= 1; i--){
      while(temp->forward[i]->time < time)
        temp = temp->forward[i];
      update[i] = temp;
    }
    
    temp = temp->forward[1];
    
    //appending of OS_RECTASKINFO if time node exists
    if (time == temp->time) {
      append_TCB(temp, task);
      return;
    } else {
      //adding of time node into the Skip List
      level = rand_level();
      if (level > list->level) {
        //updating the new node that is above the existing level
        for(i = list->level+1;i<=level;i++){
          update[i] = NULL;
        }
        list->level = level;
      }
      
      //updating the new time node and inserting the OS_RECTASKINFO
      temp = (OS_RECNODE *) OSMemGet((OS_MEM *)&RecursiveNodePartition,
				     (OS_ERR *)&err);
      temp->time = time;
      append_TCB(temp, task);
      temp->forward = (OS_RECNODE **) OSMemGet((OS_MEM *)&SkipListForwardPartition,
				               (OS_ERR *)&err);
      //updating the forward node that is below the previous level index
      for(i=1;i<=level;i++){
        temp->forward[i] = update[i]->forward[i];
        update[i]->forward[i] = temp;
      }
    }
    return;   
}

void append_TCB(OS_RECNODE *node, OS_RECTASKINFO *task)
{
    OS_RECTASKINFO *temp = node->task->next;
    OS_ERR err;
    
    //insertion of OS_RECTASKINFO if there are no OS_RECTASKINFO in the Skip List node
    if(temp == NULL) {
      temp = (OS_RECTASKINFO *) OSMemGet((OS_MEM *)&RecursiveTaskInfoPartition,
                                         (OS_ERR *)&err);
      temp->p_tcb = task->p_tcb;
      temp->p_name = task->p_name;
      temp->p_task = task->p_task;
      temp->p_arg = task->p_arg;
      temp->prio = task->prio;
      temp->p_stk_base = task->p_stk_base;
      temp->stk_limit = task->stk_limit;
      temp->stk_size = task->stk_size;
      temp->q_size = task->q_size;
      temp->time_quanta = task->time_quanta;
      temp->p_ext = task->p_ext;
      temp->opt = task->opt;
      temp->p_err = task->p_err;
      temp->p_period = task->p_period;
      temp->next = NULL;
      return;
      
    } else {
      
      while (temp->next != NULL) {
        temp = temp->next;
      }
      temp->next = (OS_RECTASKINFO *) OSMemGet((OS_MEM *)&RecursiveTaskInfoPartition,
                                               (OS_ERR *)&err);
      temp->p_tcb = task->p_tcb;
      temp->p_name = task->p_name;
      temp->p_task = task->p_task;
      temp->p_arg = task->p_arg;
      temp->prio = task->prio;
      temp->p_stk_base = task->p_stk_base;
      temp->stk_limit = task->stk_limit;
      temp->stk_size = task->stk_size;
      temp->q_size = task->q_size;
      temp->time_quanta = task->time_quanta;
      temp->p_ext = task->p_ext;
      temp->opt = task->opt;
      temp->p_err = task->p_err;
      temp->p_period = task->p_period;
      temp->next = NULL;
      return;
    }
}

int rand_level()
{
  int level = 1;
  //using random number generator to create a probablitistic multi-level node creation
  while(rand() < RAND_MAX/2 && level < SKIPLIST_MAX_LEVEL)
    level++;
  return level;
}

void skiplist_delete(OS_REC_SKIPLIST *list, int time)
{
    int i;
    OS_RECNODE *update[SKIPLIST_MAX_LEVEL + 1];
    OS_RECNODE *temp = list->header;
    
    //traverse the Skiplist to go to locate the time node to delete
    for(i=list->level;i>=1;i++){
      while(temp->forward[i]->time < time)
        temp = temp->forward[i];
      update[i] = temp;
    }
    
    temp = temp->forward[1];
    
    if (temp->time == time) {
      //traversing the level of a OS_RECNODE to amend the pointer
      for(i=1;i<=list->level;i++){
        //amending the OS_RECNODE to point to the next next OS_RECNODE
        //to account for multi-level OS_RECNODE point to single level OS_RECNODE
        //and single level OS_RECNODE pointing to multi-level OS_RECNODE
        if(update[i]->forward[i] != temp)
          break;
        update[i]->forward[i] = temp->forward[i];
      }
      
      //deletion of temp node
      skiplist_node_free(temp);
      
      //update the the level
      while(list->level > 1 && list->header->forward[list->level] == NULL)
        list->level--;
      return;
    }
    return;
}

void skiplist_node_free(OS_RECNODE *node)
{ 
  OS_ERR err;
  
  if(node) {  
    OSMemPut((OS_MEM *)&SkipListForwardPartition,
	     (void *)node->forward,
             (OS_ERR *)&err);
    
    OSMemPut((OS_MEM *)&RecursiveNodePartition,
	     (void *)node,
             (OS_ERR *)&err);
  }
}

/************************************************************************************
                      AVL TREE AND EDF SCHEDULING CODE
************************************************************************************/

                        
void EDFSchedInit()
{
      OS_ERR err;
      
      OSMemCreate((OS_MEM *)&EDFSchedulerPartition,
                  (CPU_CHAR *)"EDF Scheduler Partition",
                  (void *)&EDFSchedulerStorage[0],
                  (OS_MEM_QTY)APP_TASK_MAX,
                  (OS_MEM_SIZE)sizeof(OS_EDFSCHED_NODE),
                  (OS_ERR *)&err);
}

/* Convenient function for getting the greater of two numbers */
int max(int a, int b)
{
    return (a > b)? a : b;
}

/* Gets height of tree rooted at N */
int height(OS_EDFSCHED_NODE *N)
{
    if (N == NULL)
        return 0;
    return N->height;
}

/* Creation of new nodes */
OS_EDFSCHED_NODE* newNode(int deadline, OS_RECTASKINFO *newRdyTaskInfo)
{
	OS_ERR err;
	
    OS_EDFSCHED_NODE* node = (OS_EDFSCHED_NODE*) OSMemGet(	(OS_MEM *)&EDFSchedulerPartition,
								(OS_ERR *)&err);
    node->deadline = deadline;
	node->num_tasks = 1;
	node->OSRecTaskInfo[0] = newRdyTaskInfo;
	
	/* a new node is initially added as a leaf: pointing to no other nodes, with height 1 */
    node->left = NULL;
    node->right = NULL;
    node->height = 1; 
	
    return(node);
}

/*	AVL trees are always balanced (heights of left and right subtrees differing by at most 1)
	because whenever nodes are inserted or deleted, tree rebalancing takes place to ensure that
	the tree does not become unbalanced, as BSTs might if nodes are added with keys in ascending
	or descending order. The next two functions are primitives for balancing unbalanced nodes, 
	by rotating the tree rooted at that node either left or right.
	*/

/* Rotating to the right */
OS_EDFSCHED_NODE *rightRotate(OS_EDFSCHED_NODE *N1)
{
    OS_EDFSCHED_NODE *N2 = N1->left;
    OS_EDFSCHED_NODE *N3 = N2->right;
 
    // Perform rotation		
    N2->right = N1;			
    N1->left = N3;

	/* Rotation performed (x and y are other subtrees):
			N1				N2
		   /  \			   /  \
		  N2   x ----> 	  y	   N1
		 /  \				  /  \
		y    N3				 N3   x
	*/
 
    // Update heights
    N1->height = max(height(N1->left), height(N1->right))+1;
    N2->height = max(height(N2->left), height(N2->right))+1;
 
    // Return new root
    return N2;
}

/* Rotating to the left */
OS_EDFSCHED_NODE *leftRotate(OS_EDFSCHED_NODE *N1)
{
    OS_EDFSCHED_NODE *N2 = N1->right;
    OS_EDFSCHED_NODE *N3 = N2->left;
 
    // Perform rotation		
    N2->left = N1;			
    N1->right = N3;

	/* Rotation performed (x and y are other subtrees):
			N1					N2
		   /  \			   	   /  \
		  x    N2 	----> 	  N1   y
			  /  \			 /  \
		     N3	  y			x   N3
	*/
 
    // Update heights
    N1->height = max(height(N1->left), height(N1->right))+1;
    N2->height = max(height(N2->left), height(N2->right))+1;
 
    // Return new root
    return N2;
}

/* Calculate how unbalanced the AVL tree rooted at N is. 
	0 denotes a perfectly-balanced tree. */
int getBalance(OS_EDFSCHED_NODE *N)
{
    if (N == NULL)
        return 0;
    return height(N->left) - height(N->right);
}

/* Gets the node corresponding to the earliest deadline.
	Because of the assured balanced nature of AVL trees, 
	this should complete in O(log n) time. */
OS_EDFSCHED_NODE *earliestDeadlineNode(OS_EDFSCHED_NODE *rootNode)
{
    OS_EDFSCHED_NODE *currentNode = rootNode;
 
    /* loop down to find the leftmost leaf */
    while (currentNode->left != NULL)
        currentNode = currentNode->left;
 
    return currentNode;
}

/* Inserts a new node into the AVL tree.
	This function should also be used to initialise the tree
	i.e. call this function with the root node at NULL,
	to create a new root node. Returns pointer to root of tree */
OS_EDFSCHED_NODE *insertNode(OS_EDFSCHED_NODE *rootNode, 
				int deadline, OS_RECTASKINFO *newRdyTaskInfo)
{
	int balance;
	
	/* Insert node as per BST insertion */
	if (rootNode == NULL) /* new tree */
		return(newNode(deadline, newRdyTaskInfo));
	
	if (deadline < rootNode->deadline)
        rootNode->left  = insertNode(rootNode->left, deadline, newRdyTaskInfo);
    else if (deadline > rootNode->deadline)
        rootNode->right = insertNode(rootNode->right, deadline, newRdyTaskInfo);
    else /* if deadline already exists, add another task to node with this deadline */
    {
		rootNode->OSRecTaskInfo[rootNode->num_tasks] = newRdyTaskInfo;
		rootNode->num_tasks++;
		return rootNode;
	}    
	
	/* Update height of root node */
	rootNode->height = 1 + max(height(rootNode->left), 
								height(rootNode->right));
								
	/* Check if root node is unbalanced */
	balance = getBalance(rootNode);
	
	/* There are 4 possible scenarios in which a tree could be unbalanced:
		-left subtree has greater height, with right child of left subtree at greater height than left child
		-left subtree has greater height, with left child of left subtree at greater height than right child
		-right subtree has greater height, with right child of right subtree at greater height than left child
		-right subtree has greater height, with left child of right subtree at greater height than right child
	
	Different rotations should be carried out for each kind of inbalance.
	If the tree is balanced, no rotation is carried out and the root node is returned.
	*/
	
	/* Case 1 */
	if (balance > 1 && deadline > rootNode->left->deadline)
    {
        rootNode->left = leftRotate(rootNode->left);
        return rightRotate(rootNode);
    }
	/* Case 2 */
	if (balance > 1 && deadline < rootNode->left->deadline)
    {
        return rightRotate(rootNode);
    }
	/* Case 3 */
	if (balance < -1 && deadline > rootNode->right->deadline)
    {
        return leftRotate(rootNode);
    }
	/* Case 4 */
	if (balance < 1 && deadline < rootNode->right->deadline)
    {
        rootNode->right = rightRotate(rootNode->right);
        return leftRotate(rootNode);
    }
	
	/* Balanced tree */
	return rootNode;
}

/* Deletes a node in AVL tree with given deadline.
	Returns pointer to root of modified tree */
OS_EDFSCHED_NODE *deleteNode(OS_EDFSCHED_NODE *rootNode, 
				int deadline)
{
	OS_EDFSCHED_NODE *temp;
	OS_ERR err;
	int balance;
        
	/* Delete node as per BST node deletion */
	if (rootNode == NULL)
        return rootNode;
	
	if (deadline < rootNode->deadline )
        rootNode->left = deleteNode(rootNode->left, deadline);
 
    else if(deadline > rootNode->deadline )
        rootNode->right = deleteNode(rootNode->right, deadline);
	
	//At this point, the current root node is the node to be deleted
	else
    {
        //Case 1: node with one or no child
        if( (rootNode->left == NULL) || (rootNode->right == NULL) )
        {
            temp = rootNode->left ? rootNode->left : rootNode->right;
 
            //node has no child
            if (temp == NULL)
            {
                temp = rootNode;
                rootNode = NULL;
            }
            else //node has one child
				*rootNode = *temp; // Copy the contents of the non-empty child
				
            OSMemPut((OS_MEM *)&EDFSchedulerPartition,
					 (void *)temp,
					 (OS_ERR *)&err);
        }
		//Case 2: node with 2 children
        else
        {
            //Get the inorder successor (smallest in the right subtree)
            temp = earliestDeadlineNode(rootNode->right);
 
            //Copy the inorder successor's data to this node
            rootNode->deadline = temp->deadline;
 
            // Delete the inorder successor
            rootNode->right = deleteNode(rootNode->right, temp->deadline);
        }
    }
	
	/* If the tree only had one node, return now; no point carrying on */
	if (rootNode == NULL)
		return rootNode;
	
	/* Rebalancing of tree.
		This is slightly different from the same action in inserting a node.
		The choice of which rebalancing action to take is now based on how balanced
		the children of the root node are, as this rebalancing action has to be
		carried out all the way up to the root node.
		*/
		
	/* Check if root node is unbalanced */
	balance = getBalance(rootNode);
	
	/* Choose which rebalancing action to take */
	
	/* Case 1 */
	if (balance > 1 && getBalance(rootNode->left) < 0)
    {
        rootNode->left = leftRotate(rootNode->left);
        return rightRotate(rootNode);
    }
	/* Case 2 */
	if (balance > 1 && getBalance(rootNode->left) >= 0)
    {
        return rightRotate(rootNode);
    }
	/* Case 3 */
	if (balance < -1 && getBalance(rootNode->right) <= 0)
    {
        return leftRotate(rootNode);
    }
	/* Case 4 */
	if (balance < 1 && getBalance(rootNode->right) > 0)
    {
        rootNode->right = rightRotate(rootNode->right);
        return leftRotate(rootNode);
    }
	
	/* Balanced tree */
	return rootNode;
	
}

//Steps to program in EDF in AppTask
//create static variable

//set the running task pointer and current earliest deadline pointer node to the running node in the AVL tree

//start of while loop
//traverse the recursive list, compare running task deadline with the task release deadline from recursive list
//if the newly release task has earlier deadline, point the current earliest deadline pointer to the new node.
//insert the new task to AVL tree (if the node has not been created, create the deadline node in AVL tree and insert the task.
//if the newly release task is of the same deadline as the existing node, create the task in Micrcum using same priority created previously
//keep on doing this until the end recursive list

//if the runnning task pointer is not equal to current deadline pointer node, create all the tasks pointed by the current earliest deadline pointer of the higher priority

//task deletion
//In the OSTaskDelete, call AVL tree to delete and see if the earliest deadline is scheduled? check if static priority - total number of different priority have things in it.