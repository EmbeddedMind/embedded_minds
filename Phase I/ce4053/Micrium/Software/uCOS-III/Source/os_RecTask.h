
#ifndef RECTASKHEADER
#define RECTASKHEADER

#include <os.h>

#define SKIPLIST_MAX_LEVEL 5
#define APP_TASK_MAX 5
#define TASK_RELEASE (OS_FLAGS)0x0001

//For storing task information
//This needs all the arguments for a task's OSRecTaskCreate() call,
//as well as the task's period
typedef struct OS_RECTASKINFO {
	OS_TCB 	        *p_tcb; 
	CPU_CHAR        *p_name;
	OS_TASK_PTR 	 p_task;
	void	   	*p_arg;
	OS_PRIO		 prio;
	CPU_STK         *p_stk_base;
	CPU_STK_SIZE   	 stk_limit;
	CPU_STK_SIZE     stk_size;
	OS_MSG_QTY     	 q_size;
	OS_TICK        	 time_quanta;
	void            *p_ext;
	OS_OPT         	 opt;
	OS_ERR          *p_err;
	int		 p_period;
} OS_RECTASKINFO;

//AVL tree node
typedef struct OS_EDFSCHED_NODE 
{
	int deadline;
	OS_TCB *OSRecTaskInfo[APP_TASK_MAX];
	int num_tasks;
	struct OS_EDFSCHED_NODE *left;
	struct OS_EDFSCHED_NODE *right;
	int height;
} OS_EDFSCHED_NODE;
        
//skip list node
typedef struct OS_RECNODE {
    signed int time;
    int level;
    int num_task;
    OS_RECTASKINFO *task[APP_TASK_MAX];
    struct OS_RECNODE *forward[SKIPLIST_MAX_LEVEL+1];
} OS_RECNODE;

//skip list 
typedef struct OS_REC_SKIPLIST {
    int level;
    struct OS_RECNODE *tail;
    struct OS_RECNODE *header;
} OS_REC_SKIPLIST;


/* Global variables and memory partitions 
   These have been declared as external - 
   they are defined in os_RecTask.c, except for
   app_time, which is defined in app.c */

//Flag group for triggering synchronous release
extern OS_FLAG_GRP TaskReleaseFlag;

//Recursive task list
extern OS_REC_SKIPLIST *RecTaskList;

//EDF AVL Tree Declaration
extern OS_EDFSCHED_NODE *EDF_Scheduler_Ptr;

//EDF Earliest Deadline Node Pointer
extern OS_EDFSCHED_NODE  *Earliest_Deadline_Task;

/*
It is assumed that the total number of tasks will be created is not greater than APP_TASK_MAX.
Therefore, in the worst case, the AVL tree that makes up the EDF scheduler should have
APP_TASK_MAX nodes, for APP_TASK_MAX tasks that are ready to run with different deadlines.
In the same way, the number of tasks pointed to in each node cannot be greater than APP_TASK_MAX,
for even if all tasks share the same deadline and are stored in the same node, there can only be
APP_TASK_MAX tasks to be stored at most.
*/

//Storage for recursive task list
extern OS_MEM          RecursiveNodePartition;
//extern OS_RECNODE      RecursiveNodeStorage[APP_TASK_MAX * 4][APP_TASK_MAX * 4];
extern OS_RECNODE      RecursiveNodeStorage[APP_TASK_MAX+5];

//Storage for EDF scheduler ready list
extern OS_MEM EDFSchedulerPartition;
extern OS_EDFSCHED_NODE EDFSchedulerStorage[APP_TASK_MAX];

//Storage for Skiplist Recursive Task list
extern OS_MEM          RecSkiplistPartition;
extern OS_REC_SKIPLIST RecSkipListStorage[1];

//Application time value
extern int app_time;

/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

/* Recursive Task Management functions */
void OSRecTaskListInit ();
void OSRecTaskSvcInit ();
void OSRecTaskCreate (OS_TCB *p_TCB, 
                      CPU_CHAR *p_name,
                      OS_TASK_PTR p_task,
                      void *p_arg,
                      OS_PRIO prio,
                      CPU_STK *p_stk_base,
                      CPU_STK_SIZE stk_limit,
                      CPU_STK_SIZE stk_size,
                      OS_MSG_QTY q_size,
                      OS_TICK time_quanta,
                      void *p_ext,
                      OS_OPT opt,
                      OS_ERR *p_err);      
void OSRecTaskInit (OS_TCB *p_TCB, 
                    CPU_CHAR *p_name,
                    OS_TASK_PTR p_task,
                    void *p_arg,
                    OS_PRIO prio,
                    CPU_STK *p_stk_base,
                    CPU_STK_SIZE stk_limit,
                    CPU_STK_SIZE stk_size,
                    OS_MSG_QTY q_size,
                    OS_TICK time_quanta,
                    void *p_ext,
                    OS_OPT opt,
                    OS_ERR *p_err,
                    OS_RECTASKINFO *RecTask);
void OSRecTaskDel (OS_TCB *p_tcb, OS_ERR *p_err);
void OSRecTaskMgr (void *p_arg);

/* EDF Scheduler Management functions */
void EDFSchedInit(); 
void OS_EDFRdyListRemove (OS_TCB* p_tcb);
void insertEDFSched (int deadline, OS_TCB *newRdyTaskInfo);
void runEDFSched  ();    

/* Skip list manipulation functions */
void skiplist_init();
void skiplist_insert(int time, OS_RECTASKINFO *task);
void insert_task(OS_RECNODE *temp,OS_RECTASKINFO *task);
int rand_level();
void skiplist_delete(int time);
void skiplist_deletion();
void skiplist_print();

/* AVL tree manipulation functions */
int max(int a, int b);
int height(OS_EDFSCHED_NODE *N);
OS_EDFSCHED_NODE* OSNewEDFNode(int deadline, OS_TCB *newRdyTaskInfo);
OS_EDFSCHED_NODE *rightRotate(OS_EDFSCHED_NODE *N1);
OS_EDFSCHED_NODE *rightRotate(OS_EDFSCHED_NODE *N1);
int getBalance(OS_EDFSCHED_NODE *N);
OS_EDFSCHED_NODE *OSEarliestDeadlineNode(OS_EDFSCHED_NODE *rootNode);
OS_EDFSCHED_NODE *OSInsertEDFNode(OS_EDFSCHED_NODE *rootNode, int deadline, OS_TCB *newRdyTaskInfo);
OS_EDFSCHED_NODE *OSDeleteEDFNode(OS_EDFSCHED_NODE *rootNode, int deadline);
void printAVLTree(OS_EDFSCHED_NODE *rootNode);

#endif