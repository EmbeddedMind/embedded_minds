/*        
*********************************************************************************************************
                                      OS_RecTask.c
			Functionality for implementing recursive tasks
					
This file includes:
	* skip list data structure definition and functions for manipulating a skip list
	* functions for creating and deleting recursive tasks, based on skip lists
        * AVL tree data structure definition and functions for manipulating an AVL tree
        * functions for implementing EDF scheduling, based on an AVL tree
	
Task recursion is implemented using a skip list.
Tasks are added in increasing order of deadline to a skip list.
Each skip list node contains the time value for that node and a linked list describing tasks
to be created at that time.
Every second, the head of the skip list is checked for any tasks that should be released.
After that is done, the header is deleted.
The header node is always the node closest to the current time.
*********************************************************************************************************
*/
#include <os.h>

		
/*
*********************************************************************************************************
*  	void OSRecTaskListInit()
*	Initializes the skip list used for managing recursive tasks, named RecTaskList
*	Returns pointer to RecTaskList
*	If an application requires task recursion, this function should be called before OSRecTaskCreate(),
*	to create the necessary skip list first
*********************************************************************************************************
*/

void OSRecTaskListInit(OS_REC_SKIPLIST *RecTaskList)
{
    OS_ERR err;
  
    //Create memory partition for the recursive time node of the skiplist, use for deletion and insertion of time node 
    OSMemCreate((OS_MEM *)&RecursiveNodePartition,
                (CPU_CHAR *)"Recursive Node Partition",
                (void *)&RecursiveNodeStorage[0],
                (OS_MEM_QTY)APP_TASK_MAX,
                (OS_MEM_SIZE)sizeof(OS_RECNODE),
                (OS_ERR *)&err);
  
    RecTaskList = skiplist_init(RecTaskList);
	
}

/*
*********************************************************************************************************
void OSRecTaskInit     (OS_TCB        *p_tcb,
                        CPU_CHAR      *p_name,
                        OS_TASK_PTR    p_task,
                        void          *p_arg,
                        OS_PRIO        prio,
                        CPU_STK       *p_stk_base,
                        CPU_STK_SIZE   stk_limit,
                        CPU_STK_SIZE   stk_size,
                        OS_MSG_QTY     q_size,
                        OS_TICK        time_quanta,
                        void          *p_ext,
                        OS_OPT         opt,
                        OS_ERR        *p_err)

Calls OSRecTaskCreate() for the initial release of this task,
and adds a skip list entry for the next release of this task

Arguments:
This function uses the same arguments as OSTaskCreate, except that instead of giving a priority number
for prio, you should give the task period.
It should be called using the same arguments and ordering as you would for OSTaskCreate().

*********************************************************************************************************
*/
void OSRecTaskInit     (OS_TCB        *p_tcb,
                        CPU_CHAR      *p_name,
                        OS_TASK_PTR    p_task,
                        void          *p_arg,
                        OS_PRIO        prio,
                        CPU_STK       *p_stk_base,
                        CPU_STK_SIZE   stk_limit,
                        CPU_STK_SIZE   stk_size,
                        OS_MSG_QTY     q_size,
                        OS_TICK        time_quanta,
                        void          *p_ext,
                        OS_OPT         opt,
                        OS_ERR        *p_err)
{
	/* Create initial release of this task */
	OSRecTaskCreate (	p_tcb,
						p_name,
						p_task,
						p_arg,
						prio,
						p_stk_base,
						stk_limit,
						stk_size,
						q_size,
						time_quanta,
						p_ext,
						opt,
						p_err);
	
	/* Insert next release of this task into skip list */
	/* Save arguments of this function into new OS_RECTASKINFO object:
		they will be needed for subsequent OSRecTaskCreate() calls */
	/* Create new skip list node or insert this task into an existing skip list node */
	/* ADD YOUR CODE HERE */
}

/*
*********************************************************************************************************
void OSRecTaskCreate   (OS_TCB        *p_tcb,
                        CPU_CHAR      *p_name,
                        OS_TASK_PTR    p_task,
                        void          *p_arg,
                        OS_PRIO        prio,
                        CPU_STK       *p_stk_base,
                        CPU_STK_SIZE   stk_limit,
                        CPU_STK_SIZE   stk_size,
                        OS_MSG_QTY     q_size,
                        OS_TICK        time_quanta,
                        void          *p_ext,
                        OS_OPT         opt,
                        OS_ERR        *p_err)

Recursive task variant of OSTaskCreate(). Instead of inserting to the OS's ready list,
this function inserts the task to the application ready list (in this case, the EDF
scheduler's ready list). This should only be used for application tasks.

Arguments:
This function uses the same arguments as OSTaskCreate, except that instead of giving a priority number
for prio, you should give the task period.
It should be called using the same arguments and ordering as you would for OSTaskCreate().

*********************************************************************************************************
*/
void OSRecTaskCreate   (OS_TCB        *p_tcb,
                        CPU_CHAR      *p_name,
                        OS_TASK_PTR    p_task,
                        void          *p_arg,
                        OS_PRIO        prio,
                        CPU_STK       *p_stk_base,
                        CPU_STK_SIZE   stk_limit,
                        CPU_STK_SIZE   stk_size,
                        OS_MSG_QTY     q_size,
                        OS_TICK        time_quanta,
                        void          *p_ext,
                        OS_OPT         opt,
                        OS_ERR        *p_err)
{
    CPU_STK_SIZE   i;
#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    OS_OBJ_QTY     reg_nbr;
#endif
    CPU_STK       *p_sp;
    CPU_STK       *p_stk_limit;
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* ---------- CANNOT CREATE A TASK FROM AN ISR ---------- */
        *p_err = OS_ERR_TASK_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u                                  /* ---------------- VALIDATE ARGUMENTS ------------------ */
    if (p_tcb == (OS_TCB *)0) {                             /* User must supply a valid OS_TCB                        */
        *p_err = OS_ERR_TCB_INVALID;
        return;
    }
    if (p_task == (OS_TASK_PTR)0) {                         /* User must supply a valid task                          */
        *p_err = OS_ERR_TASK_INVALID;
        return;
    }
    if (p_stk_base == (CPU_STK *)0) {                       /* User must supply a valid stack base address            */
        *p_err = OS_ERR_STK_INVALID;
        return;
    }
    if (stk_size < OSCfg_StkSizeMin) {                      /* User must supply a valid minimum stack size            */
        *p_err = OS_ERR_STK_SIZE_INVALID;
        return;
    }
    if (stk_limit >= stk_size) {                            /* User must supply a valid stack limit                   */
        *p_err = OS_ERR_STK_LIMIT_INVALID;
        return;
    }
#endif

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
    if (prio == (OS_PRIO)0) {
        if (p_tcb != &OSIntQTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use priority 0                          */
            return;
        }
    }
#endif

    if (prio == (OS_CFG_PRIO_MAX - 1u)) {
        if (p_tcb != &OSIdleTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use same priority as idle task          */
            return;
        }
    }

    OS_TaskInitTCB(p_tcb);                                  /* Initialize the TCB to default values                   */

    *p_err = OS_ERR_NONE;
                                                            /* --------------- CLEAR THE TASK'S STACK --------------- */
    if ((opt & OS_OPT_TASK_STK_CHK) != (OS_OPT)0) {         /* See if stack checking has been enabled                 */
        if ((opt & OS_OPT_TASK_STK_CLR) != (OS_OPT)0) {     /* See if stack needs to be cleared                       */
            p_sp = p_stk_base;
            for (i = 0u; i < stk_size; i++) {               /* Stack grows from HIGH to LOW memory                    */
                *p_sp = (CPU_STK)0;                         /* Clear from bottom of stack and up!                     */
                p_sp++;
            }
        }
    }
                                                            /* ------- INITIALIZE THE STACK FRAME OF THE TASK ------- */
#if (CPU_CFG_STK_GROWTH == CPU_STK_GROWTH_HI_TO_LO)
    p_stk_limit = p_stk_base + stk_limit;
#else
    p_stk_limit = p_stk_base + (stk_size - 1u) - stk_limit;
#endif

    p_sp = OSTaskStkInit(p_task,
                         p_arg,
                         p_stk_base,
                         p_stk_limit,
                         stk_size,
                         opt);

                                                            /* -------------- INITIALIZE THE TCB FIELDS ------------- */
    p_tcb->TaskEntryAddr = p_task;                          /* Save task entry point address                          */
    p_tcb->TaskEntryArg  = p_arg;                           /* Save task entry argument                               */

    p_tcb->NamePtr       = p_name;                          /* Save task name                                         */

    p_tcb->Prio          = prio;                            /* Save the task's priority                               */

    p_tcb->StkPtr        = p_sp;                            /* Save the new top-of-stack pointer                      */
    p_tcb->StkLimitPtr   = p_stk_limit;                     /* Save the stack limit pointer                           */

    p_tcb->TimeQuanta    = time_quanta;                     /* Save the #ticks for time slice (0 means not sliced)    */
#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
    if (time_quanta == (OS_TICK)0) {
        p_tcb->TimeQuantaCtr = OSSchedRoundRobinDfltTimeQuanta;
    } else {
        p_tcb->TimeQuantaCtr = time_quanta;
    }
#endif
    p_tcb->ExtPtr        = p_ext;                           /* Save pointer to TCB extension                          */
    p_tcb->StkBasePtr    = p_stk_base;                      /* Save pointer to the base address of the stack          */
    p_tcb->StkSize       = stk_size;                        /* Save the stack size (in number of CPU_STK elements)    */
    p_tcb->Opt           = opt;                             /* Save task options                                      */

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    for (reg_nbr = 0u; reg_nbr < OS_CFG_TASK_REG_TBL_SIZE; reg_nbr++) {
        p_tcb->RegTbl[reg_nbr] = (OS_REG)0;
    }
#endif

#if OS_CFG_TASK_Q_EN > 0u
    OS_MsgQInit(&p_tcb->MsgQ,                               /* Initialize the task's message queue                    */
                q_size);
#endif

    OSTaskCreateHook(p_tcb);                                /* Call user defined hook                                 */

                                                            /* --------------- ADD TASK TO READY LIST --------------- */
                                                            /* This part differs from OSTaskCreate()*/
    OS_CRITICAL_ENTER();
    insertEDFSched(app_time + prio, p_tcb);                 /* Insert the task into the EDF ready list
                                                              Deadline calculated from adding the period (i.e. relative deadline) to the current time */
    
#if OS_CFG_DBG_EN > 0u
    OS_TaskDbgListAdd(p_tcb);
#endif

    OSTaskQty++;                                            /* Increment the #tasks counter                           */

    if (OSRunning != OS_STATE_OS_RUNNING) {                 /* Return if multitasking has not started                 */
        OS_CRITICAL_EXIT();
        return;
    }

    OS_CRITICAL_EXIT_NO_SCHED();

    OSSched();
}

/*
*********************************************************************************************************
void OSRecTaskDel	(OS_TCB  *p_tcb,
					 OS_ERR  *p_err)

Recursive task variant of OSTaskDel(). Instead of removing from the OS's ready list,
this function removes the task from the application ready list (in this case, the EDF
scheduler's ready list). This should only be used for application tasks.

Arguments:
This function uses the same arguments as OSTaskDel.
Just like OSTaskDel(), an application task can delete itself by calling this function with p_tcb = 0
It should be called using the same arguments and ordering as you would for OSTaskCreate().			
*********************************************************************************************************
*/
void  OSRecTaskDel (OS_TCB  *p_tcb,
					OS_ERR  *p_err)
{
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* See if trying to delete from ISR                       */
       *p_err = OS_ERR_TASK_DEL_ISR;
        return;
    }
#endif

    if (p_tcb == &OSIdleTaskTCB) {                          /* Not allowed to delete the idle task                    */
        *p_err = OS_ERR_TASK_DEL_IDLE;
        return;
    }

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
    if (p_tcb == &OSIntQTaskTCB) {                          /* Cannot delete the ISR handler task                     */
        *p_err = OS_ERR_TASK_DEL_INVALID;
        return;
    }
#endif

    if (p_tcb == (OS_TCB *)0) {                             /* Delete 'Self'?                                         */
        CPU_CRITICAL_ENTER();
        p_tcb  = OSTCBCurPtr;                               /* Yes.                                                   */
        CPU_CRITICAL_EXIT();
    }

    OS_CRITICAL_ENTER();
    switch (p_tcb->TaskState) {
        case OS_TASK_STATE_RDY:
             OS_EDFRdyListRemove(p_tcb);						/* Actual task deletion: this line is different from OSTaskDel() */
             break;

        case OS_TASK_STATE_SUSPENDED:
             break;

        case OS_TASK_STATE_DLY:                             /* Task is only delayed, not on any wait list             */
        case OS_TASK_STATE_DLY_SUSPENDED:
             OS_TickListRemove(p_tcb);
             break;

        case OS_TASK_STATE_PEND:
        case OS_TASK_STATE_PEND_SUSPENDED:
        case OS_TASK_STATE_PEND_TIMEOUT:
        case OS_TASK_STATE_PEND_TIMEOUT_SUSPENDED:
             OS_TickListRemove(p_tcb);
             switch (p_tcb->PendOn) {                       /* See what we are pending on                             */
                 case OS_TASK_PEND_ON_NOTHING:
                 case OS_TASK_PEND_ON_TASK_Q:               /* There is no wait list for these two                    */
                 case OS_TASK_PEND_ON_TASK_SEM:
                      break;

                 case OS_TASK_PEND_ON_FLAG:                 /* Remove from wait list                                  */
                 case OS_TASK_PEND_ON_MULTI:
                 case OS_TASK_PEND_ON_MUTEX:
                 case OS_TASK_PEND_ON_Q:
                 case OS_TASK_PEND_ON_SEM:
                      OS_PendListRemove(p_tcb);
                      break;

                 default:
                      break;
             }
             break;

        default:
            OS_CRITICAL_EXIT();
            *p_err = OS_ERR_STATE_INVALID;
            return;
    }

#if OS_CFG_TASK_Q_EN > 0u
    (void)OS_MsgQFreeAll(&p_tcb->MsgQ);                     /* Free task's message queue messages                     */
#endif

    OSTaskDelHook(p_tcb);                                   /* Call user defined hook                                 */

#if OS_CFG_DBG_EN > 0u
    OS_TaskDbgListRemove(p_tcb);
#endif
    OSTaskQty--;                                            /* One less task being managed                            */

    OS_TaskInitTCB(p_tcb);                                  /* Initialize the TCB to default values                   */
    p_tcb->TaskState = (OS_STATE)OS_TASK_STATE_DEL;         /* Indicate that the task was deleted                     */

    OS_CRITICAL_EXIT_NO_SCHED();
    
    /* Find the current runnning earliest deadline */
    
    
    OSSched();                                              /* Find new highest priority task                         */

    *p_err = OS_ERR_NONE;
}

/*
*********************************************************************************************************
void OSRecTaskMgr	(OS_TMR *p_tmr, void *RecTaskList)

This function is used as a callback function for the periodic timer which expires every 1 second.
When called, the recursive task list is checked at the header node, to see if there are tasks to be released. 
If there are, the tasks are created and new entries are inserted in the skip list for the next release of these tasks.
The header node is then deleted.

Arguments:
p_tmr: pointer to the timer whose callback function is this function. Not used here, but a requirement
        of the OSTmrCreate() API
p_arg: pointer to recursive task skip list. This has to be supplied in the p_arg argument when
             the timer is created.			
*********************************************************************************************************
*/
void OSRecTaskMgr       (OS_TMR *p_tmr, void *p_arg)
{
	OS_ERR *p_err;
	static int current_time = 0;
	OS_RECTASKINFO *task_to_release;
	OS_REC_SKIPLIST *RecTaskList = (OS_REC_SKIPLIST *)p_arg;
        int i = 0;
	
	app_time++;
	
	/* if there are tasks to be released at this time,
			the skip list header node's time value would be 
			equal to the current time */
	if (RecTaskList -> header -> time == current_time) 
	{
		/* create and release tasks as listed in header node,
				and insert skip list node for next release of these tasks*/
		task_to_release = RecTaskList -> header -> task[i];
						
		while (task_to_release != NULL)
		{
				OSRecTaskCreate (	task_to_release -> p_tcb,
									task_to_release -> p_name,
									task_to_release -> p_task,
									task_to_release -> p_arg,
									task_to_release -> prio,
									task_to_release -> p_stk_base,
									task_to_release -> stk_limit,
									task_to_release -> stk_size,
									task_to_release -> q_size,
									task_to_release -> time_quanta,
									task_to_release -> p_ext,
									task_to_release -> opt,
									task_to_release -> p_err);
				
				/* In inserting the skip list node for the task's next release,
						it is assumed that the OS_RECTASKINFO structure will not be deleted.
						This removes the need for duplicating this OS_RECTASKINFO structure
						before wiping it out, which means more malloc() and free() calls
						and possibly more memory fragmentation. If this assumption is wrong,
						the function for deleting a node in the skip list will need to be
						altered.
						
						Using this assumption, when inserting the new skip list node,
						the same pointer to a OS_RECTASKINFO structure can be used throughout
						for each task. The only value in this structure that should change throughout
						new insertions is the pointer to the next OS_RECTASKINFO structure, but this
						is taken care of in the append_TCB() function, as all newly-appended structures
						have their next pointer values set to NULL since they are appended at the end.
				*/
				
				skiplist_insert(RecTaskList, 
								app_time + task_to_release -> p_period, 
								task_to_release);
				
				task_to_release = task_to_release -> next;
		}	/* all tasks created, next releases scheduled in skip list */
		
		/* delete current header node */
		skiplist_delete(RecTaskList, current_time);
			
	}
					
	/* cycle complete, delay for 1 second and then check again */
}

OS_REC_SKIPLIST *skiplist_init(OS_REC_SKIPLIST *list)
{
    int i;
    OS_ERR err;
    
    //Initialization of the Recursive Skiplist pointer
    list->level = 1;
    //Creation of first node of Skiplist
    OS_RECNODE *header = (OS_RECNODE *) OSMemGet((OS_MEM *)&RecursiveNodePartition,
				                 (OS_ERR *)&err);
    
    list->header = header;
    
    //Initialization of the first node of the Skiplist
    header->time = 0;
    header->num_tasks = 0;

    for (i = 0; i <= SKIPLIST_MAX_LEVEL;i++) {
      header->forward[i] = NULL;
    }
    
    for (i = 0;i < APP_TASK_MAX;i++){
      header->task[i] = NULL;
    }
    
    return list;
    
}

void skiplist_insert(OS_REC_SKIPLIST *list, int time, OS_RECTASKINFO *task)
{
    int level;
    OS_RECNODE *update[SKIPLIST_MAX_LEVEL + 1];
    OS_RECNODE *temp = list->header;
    OS_ERR err;
    int i;
    
    //traverse the Skiplist to the specific time node
    //if time node is not created, traverse to the place before the time node should be created
    for (i = list->level;i >= 1; i--){
      while(temp->forward[i]->time < time)
        temp = temp->forward[i];
      update[i] = temp;
    }
    
    temp = temp->forward[1];
    
    //appending of OS_RECTASKINFO if time node exists
    if (time == temp->time) {
      append_TCB(temp, task);
      return;
    } else {
      //adding of time node into the Skip List
      level = rand_level();
      if (level > list->level) {
        //updating the new node that is above the existing level
        for(i = list->level+1;i<=level;i++){
          update[i] = NULL;
        }
        list->level = level;
      }
      
      //updating the new time node and inserting the OS_RECTASKINFO
      temp = (OS_RECNODE *) OSMemGet((OS_MEM *)&RecursiveNodePartition,
				     (OS_ERR *)&err);
      temp->time = time;
      append_TCB(temp, task);
      
      //updating the forward node that is below the previous level index
      for(i=1;i<=level;i++){
        temp->forward[i] = update[i]->forward[i];
        update[i]->forward[i] = temp;
      }
    }
    return;   
}

void append_TCB(OS_RECNODE *node, OS_RECTASKINFO *task)
{
    int i;
    OS_RECTASKINFO *temp = node->task[0];
    
    //insertion of OS_RECTASKINFO if there are no OS_RECTASKINFO in the Skip List node
    if(temp == NULL) {
      temp = task;
      return;
      
    } else {
      
      for (i=0;i<APP_TASK_MAX;i++){
        if(node->task[i]==NULL){
          temp = node->task[i];
          break;
        }
      }

      temp = task;
      return;
    }
}

int rand_level()
{
  int level = 1;
  //using random number generator to create a probablitistic multi-level node creation
  while(rand() < RAND_MAX/2 && level < SKIPLIST_MAX_LEVEL)
    level++;
  return level;
}

void skiplist_delete(OS_REC_SKIPLIST *list, int time)
{
    int i;
    OS_RECNODE *update[SKIPLIST_MAX_LEVEL + 1];
    OS_RECNODE *temp = list->header;
    
    //traverse the Skiplist to go to locate the time node to delete
    for(i=list->level;i>=1;i++){
      while(temp->forward[i]->time < time)
        temp = temp->forward[i];
      update[i] = temp;
    }
    
    temp = temp->forward[1];
    
    if (temp->time == time) {
      //traversing the level of a OS_RECNODE to amend the pointer
      for(i=1;i<=list->level;i++){
        //amending the OS_RECNODE to point to the next next OS_RECNODE
        //to account for multi-level OS_RECNODE point to single level OS_RECNODE
        //and single level OS_RECNODE pointing to multi-level OS_RECNODE
        if(update[i]->forward[i] != temp)
          break;
        update[i]->forward[i] = temp->forward[i];
      }
      
      //deletion of temp node
      skiplist_node_free(temp);
      
      //update the the level
      while(list->level > 1 && list->header->forward[list->level] == NULL)
        list->level--;
      return;
    }
    return;
}

void skiplist_node_free(OS_RECNODE *node)
{ 
  OS_ERR err;
  
  if(node) {      
    OSMemPut((OS_MEM *)&RecursiveNodePartition,
	     (void *)node,
             (OS_ERR *)&err);
  }
}

/************************************************************************************
                      AVL TREE AND EDF SCHEDULING CODE
************************************************************************************/

                        
void EDFSchedInit()
{
      OS_ERR err;
      
      OSMemCreate((OS_MEM *)&EDFSchedulerPartition,
                  (CPU_CHAR *)"EDF Scheduler Partition",
                  (void *)&EDFSchedulerStorage[0],
                  (OS_MEM_QTY)APP_TASK_MAX,
                  (OS_MEM_SIZE)sizeof(OS_EDFSCHED_NODE),
                  (OS_ERR *)&err);
}

/* Convenient function for getting the greater of two numbers */
int max(int a, int b)
{
    return (a > b)? a : b;
}

/* Gets height of tree rooted at N */
int height(OS_EDFSCHED_NODE *N)
{
    if (N == NULL)
        return 0;
    return N->height;
}

/* Creation of new nodes */
OS_EDFSCHED_NODE* OSNewEDFNode(int deadline, OS_TCB *newRdyTaskInfo)
{
	OS_ERR err;
	
    OS_EDFSCHED_NODE* node = (OS_EDFSCHED_NODE*) OSMemGet(	(OS_MEM *)&EDFSchedulerPartition,
								(OS_ERR *)&err);
    node->deadline = deadline;
	node->num_tasks = 1;
	node->OSRecTaskInfo[0] = newRdyTaskInfo;
	
	/* a new node is initially added as a leaf: pointing to no other nodes, with height 1 */
    node->left = NULL;
    node->right = NULL;
    node->height = 1; 
	
    return(node);
}

/*	AVL trees are always balanced (heights of left and right subtrees differing by at most 1)
	because whenever nodes are inserted or deleted, tree rebalancing takes place to ensure that
	the tree does not become unbalanced, as BSTs might if nodes are added with keys in ascending
	or descending order. The next two functions are primitives for balancing unbalanced nodes, 
	by rotating the tree rooted at that node either left or right.
	*/

/* Rotating to the right */
OS_EDFSCHED_NODE *rightRotate(OS_EDFSCHED_NODE *N1)
{
    OS_EDFSCHED_NODE *N2 = N1->left;
    OS_EDFSCHED_NODE *N3 = N2->right;
 
    // Perform rotation		
    N2->right = N1;			
    N1->left = N3;

	/* Rotation performed (x and y are other subtrees):
			N1				N2
		   /  \			   /  \
		  N2   x ----> 	  y	   N1
		 /  \				  /  \
		y    N3				 N3   x
	*/
 
    // Update heights
    N1->height = max(height(N1->left), height(N1->right))+1;
    N2->height = max(height(N2->left), height(N2->right))+1;
 
    // Return new root
    return N2;
}

/* Rotating to the left */
OS_EDFSCHED_NODE *leftRotate(OS_EDFSCHED_NODE *N1)
{
    OS_EDFSCHED_NODE *N2 = N1->right;
    OS_EDFSCHED_NODE *N3 = N2->left;
 
    // Perform rotation		
    N2->left = N1;			
    N1->right = N3;

	/* Rotation performed (x and y are other subtrees):
			N1					N2
		   /  \			   	   /  \
		  x    N2 	----> 	  N1   y
			  /  \			 /  \
		     N3	  y			x   N3
	*/
 
    // Update heights
    N1->height = max(height(N1->left), height(N1->right))+1;
    N2->height = max(height(N2->left), height(N2->right))+1;
 
    // Return new root
    return N2;
}

/* Calculate how unbalanced the AVL tree rooted at N is. 
	0 denotes a perfectly-balanced tree. */
int getBalance(OS_EDFSCHED_NODE *N)
{
    if (N == NULL)
        return 0;
    return height(N->left) - height(N->right);
}

/* Gets the node corresponding to the earliest deadline.
	Because of the assured balanced nature of AVL trees, 
	this should complete in O(log n) time. */
OS_EDFSCHED_NODE *OSEarliestDeadlineNode(OS_EDFSCHED_NODE *rootNode)
{
    OS_EDFSCHED_NODE *currentNode = rootNode;
 
    /* loop down to find the leftmost leaf */
    while (currentNode->left != NULL)
        currentNode = currentNode->left;
 
    return currentNode;
}

/* Inserts a new node into the AVL tree.
	This function should also be used to initialise the tree
	i.e. call this function with the root node at NULL,
	to create a new root node. Returns pointer to root of tree */
OS_EDFSCHED_NODE *OSInsertEDFNode(OS_EDFSCHED_NODE *rootNode, 
				int deadline, OS_TCB *newRdyTaskInfo)
{
	int balance;
	
	/* Insert node as per BST insertion */
	if (rootNode == NULL) /* new tree */
		return(OSNewEDFNode(deadline, newRdyTaskInfo));
	
	if (deadline < rootNode->deadline)
        rootNode->left  = OSInsertEDFNode(rootNode->left, deadline, newRdyTaskInfo);
    else if (deadline > rootNode->deadline)
        rootNode->right = OSInsertEDFNode(rootNode->right, deadline, newRdyTaskInfo);
    else /* if deadline already exists, add another task to node with this deadline */
    {
		rootNode->OSRecTaskInfo[rootNode->num_tasks] = newRdyTaskInfo;
		rootNode->num_tasks++;
		return rootNode;
	}    
	
	/* Update height of root node */
	rootNode->height = 1 + max(height(rootNode->left), 
								height(rootNode->right));
								
	/* Check if root node is unbalanced */
	balance = getBalance(rootNode);
	
	/* There are 4 possible scenarios in which a tree could be unbalanced:
		-left subtree has greater height, with right child of left subtree at greater height than left child
		-left subtree has greater height, with left child of left subtree at greater height than right child
		-right subtree has greater height, with right child of right subtree at greater height than left child
		-right subtree has greater height, with left child of right subtree at greater height than right child
	
	Different rotations should be carried out for each kind of inbalance.
	If the tree is balanced, no rotation is carried out and the root node is returned.
	*/
	
	/* Case 1 */
	if (balance > 1 && deadline > rootNode->left->deadline)
    {
        rootNode->left = leftRotate(rootNode->left);
        return rightRotate(rootNode);
    }
	/* Case 2 */
	if (balance > 1 && deadline < rootNode->left->deadline)
    {
        return rightRotate(rootNode);
    }
	/* Case 3 */
	if (balance < -1 && deadline > rootNode->right->deadline)
    {
        return leftRotate(rootNode);
    }
	/* Case 4 */
	if (balance < 1 && deadline < rootNode->right->deadline)
    {
        rootNode->right = rightRotate(rootNode->right);
        return leftRotate(rootNode);
    }
	
	/* Balanced tree */
	return rootNode;
}

/* Deletes a node in AVL tree with given deadline.
	Returns pointer to root of modified tree */
OS_EDFSCHED_NODE *OSDeleteEDFNode(OS_EDFSCHED_NODE *rootNode, 
				int deadline)
{
	OS_EDFSCHED_NODE *temp;
	OS_ERR err;
	int balance;
        
	/* Delete node as per BST node deletion */
	if (rootNode == NULL)
        return rootNode;
	
	if (deadline < rootNode->deadline )
        rootNode->left = OSDeleteEDFNode(rootNode->left, deadline);
 
    else if(deadline > rootNode->deadline )
        rootNode->right = OSDeleteEDFNode(rootNode->right, deadline);
	
	//At this point, the current root node is the node to be deleted
	else
    {
        //Case 1: node with one or no child
        if( (rootNode->left == NULL) || (rootNode->right == NULL) )
        {
            temp = rootNode->left ? rootNode->left : rootNode->right;
 
            //node has no child
            if (temp == NULL)
            {
                temp = rootNode;
                rootNode = NULL;
            }
            else //node has one child
				*rootNode = *temp; // Copy the contents of the non-empty child
				
            OSMemPut((OS_MEM *)&EDFSchedulerPartition,
					 (void *)temp,
					 (OS_ERR *)&err);
        }
		//Case 2: node with 2 children
        else
        {
            //Get the inorder successor (smallest in the right subtree)
            temp = OSEarliestDeadlineNode(rootNode->right);
 
            //Copy the inorder successor's data to this node
            rootNode->deadline = temp->deadline;
 
            // Delete the inorder successor
            rootNode->right = OSDeleteEDFNode(rootNode->right, temp->deadline);
        }
    }
	
	/* If the tree only had one node, return now; no point carrying on */
	if (rootNode == NULL)
		return rootNode;
	
	/* Rebalancing of tree.
		This is slightly different from the same action in inserting a node.
		The choice of which rebalancing action to take is now based on how balanced
		the children of the root node are, as this rebalancing action has to be
		carried out all the way up to the root node.
		*/
		
	/* Check if root node is unbalanced */
	balance = getBalance(rootNode);
	
	/* Choose which rebalancing action to take */
	
	/* Case 1 */
	if (balance > 1 && getBalance(rootNode->left) < 0)
    {
        rootNode->left = leftRotate(rootNode->left);
        return rightRotate(rootNode);
    }
	/* Case 2 */
	if (balance > 1 && getBalance(rootNode->left) >= 0)
    {
        return rightRotate(rootNode);
    }
	/* Case 3 */
	if (balance < -1 && getBalance(rootNode->right) <= 0)
    {
        return leftRotate(rootNode);
    }
	/* Case 4 */
	if (balance < 1 && getBalance(rootNode->right) > 0)
    {
        rootNode->right = rightRotate(rootNode->right);
        return leftRotate(rootNode);
    }
	
	/* Balanced tree */
	return rootNode;
	
}

/*****************************************************************************
void OS_EDFRdyListRemove (OS_TCB* p_tcb)

This function is the recursive task variant of OS_RdyListRemove() in the OS's scheduler.
It removes the pointer to the task's TCB from the array of TCB pointers 
in the earliest deadline node of the EDF ready list, so that the EDF scheduler
no longer schedules this task. Also deletes the earliest deadline node,
if there are now no more tasks in that node
*****************************************************************************/

void OS_EDFRdyListRemove (OS_TCB* p_tcb)
{
	int i = 0, j;
	
	/* get earliest deadline node, I assume it is named earliestDeadlineNode */
	Earliest_Deadline_Task = OSEarliestDeadlineNode(EDF_Scheduler_Ptr);
	
	/* scan array of TCB pointers for p_tcb */
	while (Earliest_Deadline_Task -> OSRecTaskInfo[i] != p_tcb)
		i++;
	
	/* p_tcb found, commence its deletion */
	/* Decrease number of tasks stored in this node */
	Earliest_Deadline_Task -> num_tasks--;
	
	/* if no more tasks are left in this node, delete this node and return */
	if(Earliest_Deadline_Task -> num_tasks == 0){
          OSDeleteEDFNode(EDF_Scheduler_Ptr, EDF_Scheduler_Ptr->deadline);
          return;
        }
	
	/* There are still tasks left in this node, 
		so delete p_tcb, by shifting other tasks forward */
	for (j = i; j < Earliest_Deadline_Task -> num_tasks; j++)
		Earliest_Deadline_Task -> OSRecTaskInfo[j] = Earliest_Deadline_Task -> OSRecTaskInfo[j + 1];
	
	/* Done: p_tcb is no longer in the array of TCB pointers,
		EDF scheduler will not schedule it */
}

void insertEDFSched(int deadline, OS_TCB *newRdyTaskInfo)
{
  EDF_Scheduler_Ptr = OSInsertEDFNode(EDF_Scheduler_Ptr, deadline, newRdyTaskInfo);
}