/*
*********************************************************************************************************
*                                              EXAMPLE CODE
*
*                          (c) Copyright 2009-2010; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*
* Filename      : app.c
* Version       : V1.00
* Programmer(s) : FUZZI
*
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#include <includes.h>
#include "driverlib/timer.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"

#include <os_RecTask.h>

/*
*********************************************************************************************************
*                                             LOCAL DEFINES
*********************************************************************************************************
*/

#define ONESECONDTICK             7000000

#define TASK1PERIOD                   10
#define TASK2PERIOD                   20


#define WORKLOAD1                     3
#define WORKLOAD2                     2


#define TIMERDIV                      (BSP_CPUClkFreq() / (CPU_INT32U)OSCfg_TickRate_Hz)

/*
*********************************************************************************************************
*                                            LOCAL VARIABLES
*********************************************************************************************************
*/

/* Resources for application tasks*/ 
static  OS_TCB       AppTaskStartTCB;
static  CPU_STK      AppTaskStartStk[APP_TASK_START_STK_SIZE];

static  OS_TCB       LeftLEDBlinkTCB;
static  CPU_STK      leftLEDBlinkStk[LEFT_LED_BLINK_STK_SIZE];

static  OS_TCB       RightLEDBlinkTCB;
static  CPU_STK      rightLEDBlinkStk[RIGHT_LED_BLINK_STK_SIZE];

static  OS_TCB       LEDBlinkTCB;
static  CPU_STK      LEDBlinkStk[LED_BLINK_STK_SIZE];

static  OS_TCB       MoveForwardTCB;
static  CPU_STK      moveFowardStk[MOVE_FORWARD_STK_SIZE];

static  OS_TCB       MoveBackwardTCB;
static  CPU_STK      moveBackwardStk[MOVE_BACKWARD_STK_SIZE];

static  OS_RECTASKINFO  LeftLEDBlinkTaskInfo;
static  OS_RECTASKINFO  RightLEDBlinkTaskInfo;
static  OS_RECTASKINFO  LEDBlinkTaskInfo;
static  OS_RECTASKINFO  ForwardTaskInfo;
static  OS_RECTASKINFO  BackwardTaskInfo;

CPU_INT32U      iCnt = 0;
CPU_INT08U      Left_tgt;
CPU_INT08U      Right_tgt;
CPU_INT32U      iToken  = 0;
CPU_INT32U      iCounter= 1;
CPU_INT32U      iMove   = 10;
CPU_INT32U      measure=0;

int app_time;

/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static  void        AppRobotMotorDriveSensorEnable    ();
        void        IntWheelSensor                    ();
        void        RoboTurn                          (tSide dir, CPU_INT16U seg, CPU_INT16U speed);
        
static  void        AppTaskStart                      (void  *p_arg);
static  void        leftLEDBlink                      (void  *p_arg);
static  void        rightLEDBlink                     (void  *p_arg);
static  void        LEDBlink                          (void  *p_arg);
static  void        moveForward                       (void  *p_arg);
static  void        moveBackward                      (void  *p_arg);

/* 	ALL SKIP LIST-RELATED CODE SHOULD BE MOVED TO os_RecTask.c AND HAS BEEN COMMENTED OUT.
	SUCH CODE WILL BE DELETED BEFORE SUBMISSION, TO KEEP THINGS NEAT.
	REMEMBER TO CREATE CORRESPONDING HEADER FILE OR EDIT EXISTING HEADER FILES FOR os_RecTask.c
 
        void        OSRecTask                         ();        
        void        skiplist_init                     (skiplist *list);
        void        skiplist_insert                   (skiplist *list, int time, OS_RECTASKINFO *task);
        void        append_TCB                        (snode *node, OS_RECTASKINFO *task);
        void        rand_level                        (int *level);
        void        skiplist_delete                   (skiplist *list, int time);
        void        skiplist_node_free                (snode *node);
        void        skiplist_search                   (skiplist *list, int time, OS_RECTASKINFO *task);
*/

/*
*********************************************************************************************************
*                                                main()
*
* Description : This is the standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary initialization.
*
* Arguments   : none
*
* Returns     : none
*********************************************************************************************************
*/

        
int  main (void)
{
    OS_ERR  err;

    BSP_IntDisAll();                                            /* Disable all interrupts.                              */
    OSInit(&err);                                               /* Init uC/OS-III.                                      */

    OSTaskCreate((OS_TCB     *)&AppTaskStartTCB,           /* Create the start task                                */
                 (CPU_CHAR   *)"App Task Start",
                 (OS_TASK_PTR ) AppTaskStart,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_START_PRIO,
                 (CPU_STK    *)&AppTaskStartStk[0],
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE / 10u,
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE,
                 (OS_MSG_QTY  ) 0u,
                 (OS_TICK     ) 0u,
                 (void       *) (CPU_INT32U) 0, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err);

    OSStart(&err);                                              /* Start multitasking (i.e. give control to uC/OS-III). */
}


/*
*********************************************************************************************************
*                                          STARTUP TASK
*
* Description : This is an example of a startup task.  As mentioned in the book's text, you MUST
*               initialize the ticker only once multitasking has started.
*
* Arguments   : p_arg   is the argument passed to 'AppTaskStart()' by 'OSTaskCreate()'.
*
* Returns     : none
*
* Notes       : 1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                  used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

static  void  AppTaskStart (void  *p_arg)
{
    CPU_INT32U  clk_freq;
    CPU_INT32U  cnts;
    OS_ERR      err;
    
    BSP_Init();                                                 /* Initialize BSP functions                             */
    CPU_Init();                                                 /* Initialize the uC/CPU services                       */
    clk_freq = BSP_CPUClkFreq();                                /* Determine SysTick reference freq.                    */
    cnts     = clk_freq / (CPU_INT32U)OSCfg_TickRate_Hz;        /* Determine nbr SysTick increments                     */
    OS_CPU_SysTickInit(cnts);                                   /* Init uC/OS periodic time src (SysTick).              */
    CPU_TS_TmrFreqSet(clk_freq);
    
    /* Enable Wheel ISR Interrupt */
    AppRobotMotorDriveSensorEnable();
    
    
    /* For periodic task support, the following initializations are needed:
          -Creating flag group
          -Creating memory partitions for data structure storage
          -Creating OSRecTaskMgr() as system task for managing releases of periodic tasks
    
    Call OSRecTaskSvcInit() to carry out these initialisations
    */
    OSRecTaskSvcInit();
    
    
    /* Initialise periodic tasks.
    Call OSRecTaskInit() for each task that is periodic.
    OSRecTaskInit() creates the first release of the task in the application task ready list,
    and schedules the next release of the task in the periodic task list. 
    
    Calling OSRecTaskInit() is like calling OSTaskCreate(), except for two differences:
    1) the field for task priority should be filled in with the task's period instead
    2) An extra argument is needed: the */
    OSRecTaskInit((OS_TCB *)&LeftLEDBlinkTCB,  
                  (CPU_CHAR *)"Blink Left LED",
                  (OS_TASK_PTR) leftLEDBlink, 
                  (void *) 0,  
                  (OS_PRIO) 5, 
                  (CPU_STK *)&leftLEDBlinkStk[0],   
                  (CPU_STK_SIZE) LEFT_LED_BLINK_STK_SIZE / 10u,  
                  (CPU_STK_SIZE) LEFT_LED_BLINK_STK_SIZE, 
                  (OS_MSG_QTY) 0u,  
                  (OS_TICK) 0u, 
                  (void *) (CPU_INT32U) 1, 
                  (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                  (OS_ERR *)&err,
                  (OS_RECTASKINFO *)&LeftLEDBlinkTaskInfo);
    OSRecTaskInit((OS_TCB *)&RightLEDBlinkTCB, 
                  (CPU_CHAR *)"Blink Right LED", 
                  (OS_TASK_PTR) rightLEDBlink, 
                  (void *) 0, 
                  (OS_PRIO) 20, 
                  (CPU_STK *)&rightLEDBlinkStk[0], 
                  (CPU_STK_SIZE) RIGHT_LED_BLINK_STK_SIZE / 10u, 
                  (CPU_STK_SIZE) RIGHT_LED_BLINK_STK_SIZE, 
                  (OS_MSG_QTY) 0u, 
                  (OS_TICK) 0u, 
                  (void *) (CPU_INT32U) 2, 
                  (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                  (OS_ERR *)&err,
                  (OS_RECTASKINFO *)&RightLEDBlinkTaskInfo);
    OSRecTaskInit((OS_TCB *)&LEDBlinkTCB,      
                  (CPU_CHAR *)"Blink Both LED",  
                  (OS_TASK_PTR) LEDBlink, 
                  (void *) 0,      
                  (OS_PRIO) 15, 
                  (CPU_STK *)&LEDBlinkStk[0],            
                  (CPU_STK_SIZE) LED_BLINK_STK_SIZE / 10u,       
                  (CPU_STK_SIZE) LED_BLINK_STK_SIZE, 
                  (OS_MSG_QTY) 0u,       
                  (OS_TICK) 0u, 
                  (void *) (CPU_INT32U) 3, 
                  (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                  (OS_ERR *)&err,
                  (OS_RECTASKINFO *)&LEDBlinkTaskInfo);
    OSRecTaskInit((OS_TCB *)&MoveForwardTCB,   
                  (CPU_CHAR *)"Move Foward",     
                  (OS_TASK_PTR) moveForward, 
                  (void *) 0,   
                  (OS_PRIO) 35, 
                  (CPU_STK *)&moveFowardStk[0],       
                  (CPU_STK_SIZE) MOVE_FORWARD_STK_SIZE / 10u,    
                  (CPU_STK_SIZE) MOVE_FORWARD_STK_SIZE, 
                  (OS_MSG_QTY) 0u,    
                  (OS_TICK) 0u, 
                  (void *) (CPU_INT32U) 4, 
                  (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                  (OS_ERR *)&err,
                  (OS_RECTASKINFO *)&ForwardTaskInfo);
    OSRecTaskInit((OS_TCB *)&MoveBackwardTCB,  
                  (CPU_CHAR *)"Move Backward",   
                  (OS_TASK_PTR) moveBackward, 
                  (void *) 0,  
                  (OS_PRIO) 60, 
                  (CPU_STK *)&moveBackwardStk[0],    
                  (CPU_STK_SIZE) MOVE_BACKWARD_STK_SIZE / 10u,   
                  (CPU_STK_SIZE) MOVE_BACKWARD_STK_SIZE, 
                  (OS_MSG_QTY) 0u,   
                  (OS_TICK) 0u, 
                  (void *) (CPU_INT32U) 5, 
                  (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                  (OS_ERR *)&err,
                  (OS_RECTASKINFO *)&BackwardTaskInfo);
    
    /* Post flag to allow OSRecTaskMgr() to run.
       Once OSRecTaskMgr() runs, it will trigger the start of running periodic tasks. 
       Don't forget to call OSRecTaskDel() at the end of each periodic task,
       to clear that task's release from the application task ready list
       when it has completed execution. */
     
    OSFlagPost(&TaskReleaseFlag,
               TASK_RELEASE,
               (OS_OPT)OS_OPT_POST_FLAG_SET,
               &err);
    
    /* //AVL Tree Testing code 
    
    OSRecTaskCreate((OS_TCB *)&LeftLEDBlinkTCB,  
                  (CPU_CHAR *)"Blink Left LED",
                  (OS_TASK_PTR) leftLEDBlink, 
                  (void *) 0,  
                  (OS_PRIO) 5, 
                  (CPU_STK *)&leftLEDBlinkStk[0],   
                  (CPU_STK_SIZE) LEFT_LED_BLINK_STK_SIZE / 10u,  
                  (CPU_STK_SIZE) LEFT_LED_BLINK_STK_SIZE, 
                  (OS_MSG_QTY) 0u,  
                  (OS_TICK) 0u, 
                  (void *) (CPU_INT32U) 1, 
                  (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                  (OS_ERR *)&err);
    OSRecTaskCreate((OS_TCB *)&RightLEDBlinkTCB, 
                    (CPU_CHAR *)"Blink Right LED", 
                    (OS_TASK_PTR) rightLEDBlink, 
                    (void *) 0, 
                    (OS_PRIO) 20, 
                    (CPU_STK *)&rightLEDBlinkStk[0], 
                    (CPU_STK_SIZE) RIGHT_LED_BLINK_STK_SIZE / 10u, 
                    (CPU_STK_SIZE) RIGHT_LED_BLINK_STK_SIZE, 
                    (OS_MSG_QTY) 0u, 
                    (OS_TICK) 0u, 
                    (void *) (CPU_INT32U) 2, 
                    (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                    (OS_ERR *)&err);
    OSRecTaskCreate((OS_TCB *)&LEDBlinkTCB,      
                    (CPU_CHAR *)"Blink Both LED",  
                    (OS_TASK_PTR) LEDBlink, 
                    (void *) 0,      
                    (OS_PRIO) 15, 
                    (CPU_STK *)&LEDBlinkStk[0],            
                    (CPU_STK_SIZE) LED_BLINK_STK_SIZE / 10u,       
                    (CPU_STK_SIZE) LED_BLINK_STK_SIZE, 
                    (OS_MSG_QTY) 0u,       
                    (OS_TICK) 0u, 
                    (void *) (CPU_INT32U) 3, 
                    (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                    (OS_ERR *)&err);
    OSRecTaskCreate((OS_TCB *)&MoveForwardTCB,   
                    (CPU_CHAR *)"Move Foward",     
                    (OS_TASK_PTR) moveForward, 
                    (void *) 0,   
                    (OS_PRIO) 35, 
                    (CPU_STK *)&moveFowardStk[0],       
                    (CPU_STK_SIZE) MOVE_FORWARD_STK_SIZE / 10u,    
                    (CPU_STK_SIZE) MOVE_FORWARD_STK_SIZE, 
                    (OS_MSG_QTY) 0u,    
                    (OS_TICK) 0u, 
                    (void *) (CPU_INT32U) 4, 
                    (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                    (OS_ERR *)&err);
    OSRecTaskCreate((OS_TCB *)&MoveBackwardTCB,  
                    (CPU_CHAR *)"Move Backward",   
                    (OS_TASK_PTR) moveBackward, 
                    (void *) 0,  
                    (OS_PRIO) 60, 
                    (CPU_STK *)&moveBackwardStk[0],    
                    (CPU_STK_SIZE) MOVE_BACKWARD_STK_SIZE / 10u,   
                    (CPU_STK_SIZE) MOVE_BACKWARD_STK_SIZE, 
                    (OS_MSG_QTY) 0u,   
                    (OS_TICK) 0u, 
                    (void *) (CPU_INT32U) 5, 
                    (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                    (OS_ERR *)&err);
    
    printf("Root of tree: %d\n", EDF_Scheduler_Ptr->deadline);
    printAVLTree(EDF_Scheduler_Ptr);
    
    printf("***********\n");
    printf("Deleting Left LED Blink Task\n");
    OSRecTaskDel((OS_TCB *)&LeftLEDBlinkTCB, (OS_ERR *)&err);
    
    printf("Root of tree: %d\n", EDF_Scheduler_Ptr->deadline);
    printAVLTree(EDF_Scheduler_Ptr);
    
    printf("***********\n");
    printf("Inserting Left LED Blink Task at deadline 20\n");
    OSRecTaskCreate((OS_TCB *)&LeftLEDBlinkTCB,  
                  (CPU_CHAR *)"Blink Left LED",
                  (OS_TASK_PTR) leftLEDBlink, 
                  (void *) 0,  
                  (OS_PRIO) 20, 
                  (CPU_STK *)&leftLEDBlinkStk[0],   
                  (CPU_STK_SIZE) LEFT_LED_BLINK_STK_SIZE / 10u,  
                  (CPU_STK_SIZE) LEFT_LED_BLINK_STK_SIZE, 
                  (OS_MSG_QTY) 0u,  
                  (OS_TICK) 0u, 
                  (void *) (CPU_INT32U) 1, 
                  (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                  (OS_ERR *)&err);
    
    printf("Root of tree: %d\n", EDF_Scheduler_Ptr->deadline);
    printAVLTree(EDF_Scheduler_Ptr);
    
    printf("***********\n");
    printf("Deleting Both LED Blink Task\n");
    OSRecTaskDel((OS_TCB *)&LEDBlinkTCB, (OS_ERR *)&err);
    
    printf("Root of tree: %d\n", EDF_Scheduler_Ptr->deadline);
    printAVLTree(EDF_Scheduler_Ptr);
    
    printf("***********\n");   
    printf("Inserting Both LED Blink Task at deadline 15\n");
    OSRecTaskCreate((OS_TCB *)&LEDBlinkTCB,      
                    (CPU_CHAR *)"Blink Both LED",  
                    (OS_TASK_PTR) LEDBlink, 
                    (void *) 0,      
                    (OS_PRIO) 15, 
                    (CPU_STK *)&LEDBlinkStk[0],            
                    (CPU_STK_SIZE) LED_BLINK_STK_SIZE / 10u,       
                    (CPU_STK_SIZE) LED_BLINK_STK_SIZE, 
                    (OS_MSG_QTY) 0u,       
                    (OS_TICK) 0u, 
                    (void *) (CPU_INT32U) 3, 
                    (OS_OPT)(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                    (OS_ERR *)&err);
    
    printf("Root of tree: %d\n", EDF_Scheduler_Ptr->deadline);
    printAVLTree(EDF_Scheduler_Ptr);
    
    printf("***********\n");  
    printf("Test complete\n");
    */
    
    /* Delete this task */
    OSTaskDel((OS_TCB *)0, &err);
    
}

static void leftLEDBlink (void *p_arg)
{
    OS_ERR      err; 
    CPU_INT32U  i,k,j=0;

    //printf("Left LED Blink\n");
    for(k=0; k<2; k++)
    {
      BSP_LED_Toggle(2u);
      for(i=0; i <ONESECONDTICK/2; i++)
         j = ((i * 2)+j);
    }
    
    //printf("End of Left LED Blink\n");
    OSRecTaskDel((OS_TCB *)0, &err);
}

static void rightLEDBlink (void *p_arg)
{
    OS_ERR      err;
    CPU_INT32U  i,k,j=0;
   
    //printf("Right LED Blink\n");
    for(k=0; k<2; k++)
    {
      BSP_LED_Toggle(1u);
      for(i=0; i <ONESECONDTICK/2; i++)
         j = ((i * 2)+j);
    }
    
    //printf("End of Right LED Blink\n");
    OSRecTaskDel((OS_TCB *)0, &err);
}

static void LEDBlink (void *p_arg)
{
    OS_ERR      err;
    CPU_INT32U  i,k,j=0;
   // printf("Both LED Blink\n");
    for(k=0; k<2; k++)
    {
      BSP_LED_Toggle(0u);
      for(i=0; i <ONESECONDTICK/2; i++)
         j = ((i * 2)+j);
    }
    
    //printf("End of Both LED Blink\n");
    OSRecTaskDel((OS_TCB *)0, &err);
}

static void moveForward (void *p_arg)
{
    OS_ERR      err; 
    CPU_INT32U  i,k;
    //printf("Move Forward \n");
    
    RoboTurn(FRONT, 16, 50);
    for(k=0; k<WORKLOAD1; k++)
    {
      for(i=0; i <ONESECONDTICK; i++){
      }
    }
    
    BSP_MotorStop(LEFT_SIDE);
    BSP_MotorStop(RIGHT_SIDE);
    //printf("End of Move Forward \n");
    OSRecTaskDel((OS_TCB *)0, &err);
}

static void moveBackward (void *p_arg)
{
    OS_ERR      err;
    CPU_INT32U  i,k;
   // printf("Move Backward \n");
    RoboTurn(BACK, 16, 50);
    for(k=0; k<WORKLOAD1; k++)
    {
      for(i=0; i <ONESECONDTICK; i++){
      }
    }

    BSP_MotorStop(LEFT_SIDE);
    BSP_MotorStop(RIGHT_SIDE);
    //printf("End of Move Backward \n");
    OSRecTaskDel((OS_TCB *)0, &err);
}

/*static  void  AppTaskOne (void  *p_arg)
{ 
    OS_ERR      err;
    CPU_INT32U  k, i, j;
    
    if(iMove > 0)
    {
      if(iMove%2==0)
      {  
      RoboTurn(BACK, 16, 50);
      iMove--;
      }
      else{
        RoboTurn(BACK, 16, 50);
        iMove++;
      }
    }
    
    for(k=0; k<WORKLOAD1; k++)
    {
      for(i=0; i <ONESECONDTICK; i++){
        j=2*i;
      }
     }
    
    OSTaskDel((OS_TCB *)0, &err);   

}

static  void  AppTaskTwo (void  *p_arg)
{   
    OS_ERR      err;
    CPU_INT32U  i,k,j=0;
   
    for(i=0; i <(ONESECONDTICK); i++)
    {
      j = ((i * 2) + j);
    }
    
    BSP_LED_Off(0u);
    for(k=0; k<5; k++)
    {
      BSP_LED_Toggle(0u);
      for(i=0; i <ONESECONDTICK/2; i++)
         j = ((i * 2)+j);
    }
    
    BSP_LED_Off(0u);
   OSTaskDel((OS_TCB *)0, &err);

}*/

static  void  AppRobotMotorDriveSensorEnable ()
{
    BSP_WheelSensorEnable();
    BSP_WheelSensorIntEnable(RIGHT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
    BSP_WheelSensorIntEnable(LEFT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
}


void IntWheelSensor()
{
	CPU_INT32U         ulStatusR_A;
	CPU_INT32U         ulStatusL_A;

	static CPU_INT08U CountL = 0;
	static CPU_INT08U CountR = 0;

	static CPU_INT08U data = 0;

	ulStatusR_A = GPIOPinIntStatus(RIGHT_IR_SENSOR_A_PORT, DEF_TRUE);
	ulStatusL_A = GPIOPinIntStatus(LEFT_IR_SENSOR_A_PORT, DEF_TRUE);

        if (ulStatusR_A & RIGHT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(RIGHT_IR_SENSOR_A_PORT, RIGHT_IR_SENSOR_A_PIN);           /* Clear interrupt.*/
          CountR = CountR + 1;
        }

        if (ulStatusL_A & LEFT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(LEFT_IR_SENSOR_A_PORT, LEFT_IR_SENSOR_A_PIN);
          CountL = CountL + 1;
        }

	if((CountL >= Left_tgt) && (CountR >= Right_tgt))
        {
          data = 0x11;
          Left_tgt = 0;
          Right_tgt = 0;
          CountL = 0;
          CountR = 0;
          BSP_MotorStop(LEFT_SIDE);
          BSP_MotorStop(RIGHT_SIDE);
        }
        else if(CountL >= Left_tgt)
        {
          data = 0x10;
          Left_tgt = 0;
          CountL = 0;
          BSP_MotorStop(LEFT_SIDE);
        }
        else if(CountR >= Right_tgt)
        {
          data = 0x01;
          Right_tgt = 0;
          CountR = 0;
          BSP_MotorStop(RIGHT_SIDE);
        }
        return;
}

void RoboTurn(tSide dir, CPU_INT16U seg, CPU_INT16U speed)
{
	Left_tgt = seg;
        Right_tgt = seg;

	BSP_MotorStop(LEFT_SIDE);
	BSP_MotorStop(RIGHT_SIDE);

        BSP_MotorSpeed(LEFT_SIDE, speed <<8u);
	BSP_MotorSpeed(RIGHT_SIDE,speed <<8u);

	switch(dir)
	{
            case FRONT :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case BACK :
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            case LEFT_SIDE :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case RIGHT_SIDE:
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            default:
                    BSP_MotorStop(LEFT_SIDE);
                    BSP_MotorStop(RIGHT_SIDE);
                    break;
	}

	return;
}
